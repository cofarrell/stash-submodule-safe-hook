package com.atlassian.stash.plugin.submodule;

import com.atlassian.stash.content.*;
import com.atlassian.stash.exception.NoSuchEntityException;
import com.atlassian.stash.exception.NoSuchObjectException;
import com.atlassian.stash.history.HistoryService;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.io.TypeAwareOutputSupplier;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.LineReader;

import javax.annotation.Nonnull;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Iterables.getFirst;
import static org.apache.commons.lang.StringUtils.removeEnd;

@SuppressWarnings("deprecation")
public class SubmoduleHook implements PreReceiveRepositoryHook {

    private final ContentService contentService;
    private final HistoryService historyService;
    private final NavBuilder navBuilder;
    private final RepositoryService repositoryService;

    public SubmoduleHook(ContentService contentService, HistoryService historyService, NavBuilder navBuilder,
                         RepositoryService repositoryService) {
        this.contentService = contentService;
        this.historyService = historyService;
        this.navBuilder = navBuilder;
        this.repositoryService = repositoryService;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext repositoryHookContext,
                             @Nonnull Collection<RefChange> refChanges,
                             @Nonnull HookResponse hookResponse) {

        List<Change> unpushed = getUnpushed(repositoryHookContext, refChanges);
        if (!unpushed.isEmpty()) {
            hookResponse.err().println("The commit you are pushing is referencing unpushed");
            hookResponse.err().println("commits of the following submodules:");
            for (Change change : unpushed) {
                hookResponse.err().println(change.getPath() + " at " + change.getContentId());
            }
            hookResponse.err().println("You must push the submodules before pushing the project");
            return false;
        }
        return true;
    }

    private List<Change> getUnpushed(RepositoryHookContext repositoryHookContext, Collection<RefChange> refChanges) {
        URI stashUrl = URI.create(navBuilder.buildAbsolute());
        List<Change> unpushed = Lists.newArrayList();
        for (RefChange refChange : refChanges) {
            // Ignore deleted refs
            if (refChange.getType() == RefChangeType.DELETE) {
                continue;
            }
            // Is it faster to do walk the file tree first? How often does a repository have a .gitmodules file? Questions...
            final Map<String, URI> gitModule = getGitModule(repositoryHookContext, refChange);
            if (gitModule.isEmpty()) {
                continue;
            }
            for (Change change : getSubmodules(refChange, repositoryHookContext.getRepository())) {
                URI submoduleUrl = gitModule.get(change.getPath().toString());
                if (submoduleUrl != null) {
                    submoduleUrl = resolveRelativePath(submoduleUrl, repositoryHookContext.getRepository());
                    if (isSameUriBestGuess(stashUrl, submoduleUrl) &&
                            !submoduleCommitExists(submoduleUrl, change.getContentId())) {
                        unpushed.add(change);
                    }
                }
            }
        }
        return unpushed;
    }

    private List<Change> getSubmodules(RefChange refChange, Repository repository) {
        final List<Change> changes = Lists.newArrayList();
        ChangesRequest.Builder request = new ChangesRequest.Builder(repository, refChange.getToHash());
        if (refChange.getType() == RefChangeType.UPDATE) {
            request = request.sinceId(refChange.getFromHash());
        }
        historyService.streamChanges(request.build(), new AbstractChangeCallback() {
            @Override
            public boolean onChange(@Nonnull Change change) throws IOException {
                if (change.getNodeType() == ContentTreeNode.Type.SUBMODULE) {
                    changes.add(change);
                }
                // Keep streaming until the end
                return true;
            }
        });
        return changes;
    }

    private boolean submoduleCommitExists(URI repoUrl, String commit) {
        String[] parts = repoUrl.getPath().split("/");
        if (parts.length > 2) { // First part is always blank due to leading slash
            Repository repo = repositoryService.getBySlug(parts[parts.length - 2], removeEnd(parts[parts.length - 1], ".git"));
            if (repo != null) {
                try {
                    //noinspection ConstantConditions
                    return historyService.getChangeset(repo, commit) != null;
                } catch (NoSuchEntityException e) {
                    return false;
                }
            }
        }
        // What happens on non-Stash submodules or missing repos?
        return true;
    }

    private Map<String, URI> getGitModule(RepositoryHookContext repositoryHookContext, RefChange refChange) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            contentService.streamFile(repositoryHookContext.getRepository(), refChange.getToHash(), ".gitmodules", new TypeAwareOutputSupplier() {
                @Override
                public OutputStream getStream(@Nonnull String contentType) throws IOException {
                    return out;
                }
            });
            Map<String, URI> submodules = Maps.newHashMap();
            for (ConfigReader.ConfigSection section : ConfigReader.readConfig(new LineReader(new StringReader(out.toString("UTF-8"))))) {
                submodules.put(getFirst(section.getEntries().get("path"), ""), URI.create(getFirst(section.getEntries().get("url"), "")));
            }
            return submodules;
        } catch (NoSuchObjectException e) {
            // Ignore, this is expected if the .gitmodules file doesn't exist
            return Collections.emptyMap();
        } catch (UnsupportedEncodingException e) {
            return Collections.emptyMap();
        } catch (IOException e) {
            return Collections.emptyMap();
        }
    }

    private static boolean isSameUriBestGuess(URI baseUrl, URI uri) {
        return baseUrl.getHost().equals(uri.getHost()) && (
                (uri.getPort() == baseUrl.getPort() && uri.getPath().startsWith(baseUrl.getPath())) ||
                        uri.getScheme().equals("ssh")
        );
    }

    private URI resolveRelativePath(URI uri, Repository repository) {
        String url = uri.toASCIIString();
        if (url.startsWith("../") || url.startsWith("./")) {
            try {
                String httpUrl = navBuilder.repo(repository).clone("git").buildAbsolute();
                // Note: The / at the end of the uri is required for the resolve to work correctly
                return new URI(httpUrl + "/").resolve(uri);
            } catch (URISyntaxException e) {
                // Never happen unless RepositoryService is sick
            }
        }
        return uri;
    }
}
