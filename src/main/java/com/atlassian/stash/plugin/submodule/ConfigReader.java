package com.atlassian.stash.plugin.submodule;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.io.LineReader;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Parses the output of a standard <a href="http://www.kernel.org/pub/software/scm/git/docs/git-config.html">Git configuration file</a>.
 * <p/>
 * Git configuration is in the following format.
 * <pre>
 * [core]
 *     repositoryformatversion = 0
 *     filemode = true
 *     bare = false
 *     logallrefupdates = true
 * [remote "origin"]
 *     fetch = +refs/heads/*:refs/remotes/origin/*
 *     url = http://admin@host:7990/stash/scm/TEST/test.git
 * [branch "master"]
 *     remote = origin
 *     merge = refs/heads/master
 * </pre>
 *
 * Taken from Stash - I'm allowed to do this because I wrote this, but in general not a good idea.
 */
public class ConfigReader {

    private static final Logger log = LoggerFactory.getLogger(ConfigReader.class);
    private static final Pattern PATTERN_SECTION = Pattern.compile("\\[([\\w|\\.|-]+|)(?: +\"([^\"]+)\")?\\]");

    public static List<ConfigSection> readConfig(LineReader reader) throws IOException {
        List<ConfigSection> sections = Lists.newArrayList();
        ConfigSection.Builder builder = null;
        ConfigValueReader lineReader = new ConfigValueReader();
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            line = line.trim();
            if (line.isEmpty() || isCommentChar(line.charAt(0))) {
                continue;
            }
            Matcher matcher = PATTERN_SECTION.matcher(line);
            if (matcher.matches()) {
                if (builder != null) {
                    sections.add(builder.build());
                }
                builder = new ConfigSection.Builder(matcher.group(1), matcher.group(2));
            } else if (builder != null) {
                String[] split = line.split("=", 2);
                if (split.length == 2) {
                    String value = split[1].trim();
                    if (StringUtils.isEmpty(value)) {
                        builder.entry(split[0].trim(), value);
                    } else {
                        builder.entry(split[0].trim(), lineReader.readAndDecodeValue(reader, value));
                    }
                } else if (split.length == 1 && !(line.contains("#") || line.contains(";"))) {
                    builder.entry(line, "true");
                } else {
                    log.warn("Invalid configuration line: '{}'", line);
                }
            }
        }
        if (builder != null) {
            sections.add(builder.build());
        }
        return sections;
    }

    private static boolean isCommentChar(char c) {
        return c == '#' || c == ';';
    }

    /**
     * A convenience class for keeping track of quote state for a single value.
     */
    private static class ConfigValueReader {

        private StringBuilder buffer;
        private boolean inQuote;
        private int lastQuote;

        /**
         * Return the decode the current line and potentially more if the value contains unclosed quotes.
         * Will reset the internal state, so this object can be re-used in a tight loop.
         */
        public String readAndDecodeValue(LineReader reader, String value) throws IOException {
            // Reset internal state
            lastQuote = 0;
            inQuote = false;
            buffer = new StringBuilder(value.length());
            readValueLines(reader, value);
            trimWhitespacesToLastQuote(buffer, lastQuote);
            return buffer.toString();
        }

        private void readValueLines(LineReader reader, String s) throws IOException {
            if (readLine(s)) {
                String line;
                while ((line = reader.readLine()) != null) {
                    if (!readLine(line)) {
                        break;
                    }
                }
            }
        }

        private boolean readLine(String value) {
            int length = value.length();
            for (int i = 0; i < length; i++) {
                char c = value.charAt(i);
                if (isCommentChar(c) && !inQuote) {
                    break;
                }
                boolean escaped = c == '\\';
                if (escaped) {
                    i++;
                    // At the end of the line but there is more
                    if (i == length) {
                        return true;
                    }
                    c = decodeEscapedCharacter(value.charAt(i));
                }
                if (c == '"' && !escaped) {
                    inQuote = !inQuote;
                    if (!inQuote) {
                        // Keep track of the last quote for correct trimming
                        lastQuote = buffer.length() - 1;
                    }
                } else {
                    // Trim whitespace at start of string when not quoted
                    if (buffer.length() > 0 || inQuote || !Character.isWhitespace(c)) {
                        buffer.append(c);
                    }
                }
            }
            // No more to read for this value
            return false;
        }

        // These are the only supported encoded characters
        private static char decodeEscapedCharacter(char c) {
            if (c == 't') {
                c = '\t';
            } else if (c == 'n') {
                c = '\n';
            } else if (c == 'b') {
                c = '\b';
            }
            return c;
        }

        private static void trimWhitespacesToLastQuote(StringBuilder buffer, int lastQuote) {
            for (int i = buffer.length() - 1; i >= 0 && Character.isWhitespace(buffer.charAt(i)) && i > lastQuote; i--) {
                buffer.setLength(i);
            }
        }
    }

    public static class ConfigSection {

        private final String name;
        private final String subsection;
        private final Multimap<String, String> entries;

        public ConfigSection(String name, String subsection, Multimap<String, String> entries) {
            this.name = checkNotNull(name, "name");
            this.subsection = subsection;
            this.entries = checkNotNull(entries, "entries");
        }

        @Nonnull
        public String getName() {
            return name;
        }

        @Nullable
        public String getSubsection() {
            return subsection;
        }

        @Nonnull
        public Multimap<String, String> getEntries() {
            return entries;
        }

        public static class Builder {
            private final ImmutableMultimap.Builder<String, String> entries;
            private String section;
            private String subsection;

            public Builder(String section, String subsection) {
                this.section = section;
                this.subsection = subsection;
                this.entries = ImmutableMultimap.builder();
            }

            public Builder entry(String name, String value) {
                entries.put(name, value);
                return this;
            }

            public ConfigSection build() {
                return new ConfigSection(section, subsection, entries.build());
            }
        }
    }

}
