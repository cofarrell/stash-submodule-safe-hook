# Safe Submodule Hook

This plugin checks for the presence of self-referencing submodules and ensures that updates point to commits that exist.
This ensures developers push submodules before pushing updates to the upstream repositories.
